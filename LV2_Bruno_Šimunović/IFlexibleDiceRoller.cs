﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_Bruno_Šimunović
{
    interface IFlexibleDiceRoller
    {
        void InsertDie(Die die);
        void RollAllDice();
    }
}
