﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_Bruno_Šimunović
{
    interface IClosedDiceRoller
    {
        void RollAllDice();
    }
}
