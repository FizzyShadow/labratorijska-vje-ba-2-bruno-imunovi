﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_Bruno_Šimunović
{
    //zadatak_1  

    //class Die
    //{
    //    private int numberOfSides;
    //    private Random randomGenerator;
    //    public Die(int numberOfSides)
    //    {
    //        this.numberOfSides = numberOfSides;
    //        this.randomGenerator = new Random();
    //    }
    //    public int Roll()
    //    {
    //        int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
    //        return rolledNumber;
    //    }
    //}

    //zadatak_2

    //class Die
    //{
    //    private int numberOfSides;
    //    private Random randomGenerator;
    //    public Die(int numberOfSides, Random randomNumber)
    //    {
    //        this.numberOfSides = numberOfSides;
    //        this.randomGenerator = randomNumber;
    //    }
    //    public int Roll()
    //    {
    //        int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
    //        return rolledNumber;
    //    }
    //}

    //zadatak_3

    //class Die
    //{
    //    private int numberOfSides;

    //    public Die(int numberOfSides)
    //    {
    //        this.numberOfSides = numberOfSides;

    //    }
    //    public int Roll()
    //    {
    //        int rolledNumber = RandomGenerator.GetInstance().NextInt(1, numberOfSides + 1);
    //        return rolledNumber;
    //    }
    //}

    class Die
    {
        private int numberOfSides;
        private Random randomGenerator;
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = new Random();
        }
        public int Roll()
        {
            int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
            return rolledNumber;
        }
        public int GetNumberOfSides
        {
            get { return this.numberOfSides; }
                
        }
        
}
    }


